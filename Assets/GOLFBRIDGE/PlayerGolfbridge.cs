﻿using UnityEngine;
using System.Collections;

namespace Golfbridge {
/// <summary>
/// Player implements input and controller extensions added to the standard assets rigidbody character controller
/// </summary>
/// <remarks>
/// A wiki article-sized thing.
/// </remarks>
public class PlayerGolfbridge : MonoBehaviour 
{

		public void LockMouse()
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
	
		void Start () 
	    {
			LockMouse ();
	
		}
	
		void Update ()
		{
			if (Input.GetMouseButton (0)) {
				LockMouse();
			}
		}
	
	}
}
