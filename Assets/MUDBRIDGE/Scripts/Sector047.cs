﻿using UnityEngine;
using System.Collections;

public class Sector047 : MonoBehaviour {
	
	public int plasmaniaMosterKill = 0;

	public int monsterRampage = 5;
	
	public string[] planets = new string[]{ "Darmak", "Eilladrid", "Kanto" };
	public string[] monsterList = new string[]{ "Plasmania", "Zmonger", "Medimak",
										     	 "Emon", "Sanker", "Medimon", 
												 "Heartmonger", "Livercod", "StomachFlipper" };
	
	public string[] lootTableList = new string[]{ "Energy Cell" };
	//public List<string> lootTableList = new List<string>();
	
	public string[] monsterMessageList = new string[]
	{ 
		"You now made the plasmanian monster mad. Damage will increase " + 
		Game.Instance.currentMonster.damage + " points!",
		"You think you can beat the Zmonger!! HAHAHAH!!!",
		"Here comes Medimak to save the day!",
		"Emon, he's easy!",
		"THE SANKER TANKER!!", 
		"Medimon, always there to give you some pain meds.",
		"Heartmonger loves to eat hearts. Put on your Ironman suit.",
		"Livercod will steal your liver fast. Quick, hide it!!"
	};
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		MonsterStats();
	}
	
	public void MonsterStats()
	{
		/// Planet Darmak
		//Pasmania
		MonsterCheck(planets[0], monsterList[0], monsterMessageList[0]);
		// Zmonger
		MonsterCheck(planets[0], monsterList[1], monsterMessageList[0]);
		
		/// Planet Eilladrid
		// Emon
		MonsterCheck(planets[1], monsterList[3], monsterMessageList[3]);
		// Sanker
		MonsterCheck(planets[1], monsterList[4], monsterMessageList[4]);
		
		/// Planet Kanto
		// Heartmonger
		MonsterCheck(planets[2], monsterList[6], monsterMessageList[6]);
		// Livercod
		MonsterCheck(planets[2], monsterList[7], monsterMessageList[7]);
	}
	
	public void MonsterCheck(string planetName, string monsterName, string monsterRant)
	{
		if(Game.Instance.currentPlaceName == planetName)
		{
			if(Game.Instance.printDelayedBufferString.Length <= 0)
			{
				if(Game.Instance.currentMonster.name == monsterName && plasmaniaMosterKill != monsterRampage)
				{
					MonsterMessage(Game.Instance.currentMonster.name);
				}
				else if(Game.Instance.currentMonster.name == monsterName && plasmaniaMosterKill == monsterRampage)
				{
					MonsterUpgrade(monsterRant);
				}
			}
		}
	}
	
	public void MonsterUpgrade(string monsterRant)
	{
		Game.Instance.currentMonster.damage += 5;
		Game.Print(monsterRant + Game.Instance.currentMonster.damage + " points!");
		plasmaniaMosterKill++;
		monsterRampage += 5;
	}
	
	public void MonsterMessage(string monsterName)
	{
		Game.Print("\"" + monsterName + "!\"");
		plasmaniaMosterKill++;
	}
}
