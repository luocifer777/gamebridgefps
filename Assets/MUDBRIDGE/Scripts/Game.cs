﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
	
	private static Game _Instance;
	public static Game Instance 
	{
		get 
		{ 
			if (_Instance == null)
			{
			   // _Instance = new Game();
			}
			return _Instance; 
		}
		
	}

    // reference to the text box
    public Text consoleText;

    public string playerName = "Professor";
    public int experiencePoints = 0;
    public int level = 1;
    public int hitPoints = 100;
    public int maxHitPoints = 100;
    public int xpNeededToLevel = 100;
    public int medPacks = 5;
    public bool dead = false;
    public int hpGainedPerLevel = 15;
    public int medPackDropChance = 30;
	public int energyCellDropChance = 30;
	public int currency = 0;
	public string currencyName = "Dhar'tariums";

    public List<Loot> lootInventoryList = new List<Loot>();

    public Loot currentWeaponLoot;

    public List<Loot> lootTableList = new List<Loot>();
	
    #region TEXT PRINTING CONSOLE
	public bool printDelayed = true;
	public string printDelayedBufferString = "";
    
    public static int numberOfLines = 0;

	public static int numberOfColumns = 80;

    public static void Print(string message)
	{
		int messageLength = message.Length;
		int numberOfLinesToAdd = messageLength / numberOfColumns; 
		if (messageLength % numberOfColumns !=0)
		{
			numberOfLines++;
	    	numberOfLinesToAdd++;
		}
		numberOfLines += numberOfLinesToAdd++;

	    if(!Instance.printDelayed)
	    {
		    // adds the new line of text to the bottom of the consoleText.text
		    Instance.consoleText.text += message + System.Environment.NewLine;
		    RemoveTopLineFromConsole();
	    }
	    else
	    {
	    	Instance.printDelayedBufferString += message + System.Environment.NewLine;
	    }
        
    }

    public int maxLines = 15;

	public static void RemoveTopLineFromConsole()
	{
		int firstNewlineCharNumber = Instance.consoleText.text.IndexOf(System.Environment.NewLine) + 2;
        // numberOfLines = Instance.consoleText.text.Split(System.Environment.NewLine.ToCharArray()).Length;
		if (numberOfLines >= Instance.maxLines)
		{
			Instance.consoleText.text = Instance.consoleText.text.Remove(0, firstNewlineCharNumber);
		numberOfLines--;
		}
	}
	
	public static void PrintBufferText()
	{
        // If we have buffered text to print
        if (Instance.printDelayedBufferString.Length > 0)
        {
            // Add the first character in the buffer
            Instance.consoleText.text += Instance.printDelayedBufferString[0];
            if (Instance.printDelayedBufferString.Length > 2)
            {
                // If the character is a new line, remove a line from the top of console
                if (Instance.printDelayedBufferString[0] == '\n')
                {
                    RemoveTopLineFromConsole();
                }
            }
            // Remove the first character of the buffer text
            Instance.printDelayedBufferString=Instance.printDelayedBufferString.Remove(0, 1);
        }
	}
	
	#endregion
	
	
	
	
	#region PLACES
	
	
	///<summary>A list of places in the game.</summary>
	public List<Place> places = new List<Place>();
	
	/// The name of the place we are at
	public string currentPlaceName = "Start";
	
	// PUBLIC/PRIVATE  TYPE OF VARIABLE   NAME OF VARIABLE   =   DEFAULT VALUE WHICH MAY BE AN OBJECT CONSTRUCTOR
	// The Place object we are at right now
	public Place currentPlace = new Place();
	
	// A FUNCTION IS:
	// public or private    return value type         NameOfFunction     (   type of argument    name of argument , another type of argument   another argument name , etc ) 
	public void GoToPlace(string destinationName)
	{
		foreach (Place newPlace in places)
		{
			if(newPlace.name == destinationName)
			{
				currentPlace = newPlace;
				currentPlaceName = newPlace.name;
			}
		}
		CheckForMonster();
		if (fighting)
		{
			FightMenuPrint();
		}
		else
		{
			PlaceMenuPrint();
		}
	}
	
	public void CheckForMonster()
	{
		if (Random.Range(0,100) <= currentPlace.monsterChance)
		{
			GenerateCurrentMonster();
		}
	}
	
	// When you hit a direction key, it finds the exit from the current places that matches and goes to that exit's destination place
	public void GoToDirection(ExitDirection intendedDirection)
	{
		Debug.Log("GOTODIRECTION");
		// Look through the current place's list of exits
		// Find an exit whose direction matches the intended direction to travel
		foreach (Exit possibleExit in currentPlace.exits)
		{
			if(possibleExit.direction == intendedDirection)
			{
				GoToPlace(possibleExit.destination);
			}
		}
	}
	
	public void PlaceMenuPrint()
	{
		Print(currentPlaceName);
		Print(currentPlace.description);
		string exitsString = "";
		foreach(Exit exit in currentPlace.exits)
		{
			exitsString += exit.direction.ToString() + ":" + exit.destination + "   ";
		}
		Print("Exits: " + exitsString);
	}
	
	public void PlaceMenuInput()
	{
		if (Input.GetKeyDown(KeyCode.N))
		{
			GoToDirection(ExitDirection.North);
		}
		if (Input.GetKeyDown(KeyCode.S))
		{
			GoToDirection(ExitDirection.South);
		}
		if (Input.GetKeyDown(KeyCode.E))
		{
			GoToDirection(ExitDirection.East);
		}
		if (Input.GetKeyDown(KeyCode.W))
		{
			GoToDirection(ExitDirection.West);
		}
		if (Input.GetKeyDown(KeyCode.U))
		{
			GoToDirection(ExitDirection.Up);
		}
		if (Input.GetKeyDown(KeyCode.D))
		{
			GoToDirection(ExitDirection.Down);
		}
		if (Input.GetKeyDown(KeyCode.G))
		{
			GoToDirection(ExitDirection.Gate);
		}
		if (Input.GetKeyDown(KeyCode.H))
		{
			GoToDirection(ExitDirection.Hatch);
		}
		if (Input.GetKeyDown(KeyCode.V))
		{
			GoToDirection(ExitDirection.Vent);
		}
	}
	#endregion
	
	
	
	
	#region MONSTERS
	
	public Monster currentMonster = new Monster("Chesthugger");

    public List<Monster> monsterList = new List<Monster>();
	
	#endregion
	
	
    #region methods

    public int points = 0;
    /// <summary>
    /// Outputs player stats
    /// </summary>
    void PlayerStatus()
    {
        Print("Player Name: " + playerName);
        Print("XP: " + experiencePoints + "/" + xpNeededToLevel);
	    Print("Level: " + level);
	    Print("HP: " + hitPoints + "/" + maxHitPoints);
	    Print(currencyName + ": " + currency);
        Print("Med Packs: " + medPacks);
        Print("Yuletronium: " + points + "/20 needed to complete mission.");
        Print("Weapon:" + currentWeaponLoot.name + " DAMAGE: " + currentWeaponLoot.damageValue + " AMMO: " + currentWeaponLoot.ammoValue);

        if (points>=20)
        {
            Print("You collected 20 Yuletronium and completed your mission!");
            Print("You return the Yuletronium to your corporate masters.");
            Print("They thank you for your services and shoot you with a smile.");
            dead = true;
        }
	   
    }
	
	public void FightMenuPrint()
	{
		
		if (!dead)
		{
			fighting=true;
			GenerateCurrentMonster();
			Print("You see a " + currentMonster.name + "!");
			Print("What do you want to do? Options are: [F] FIGHT  [H] HEAL  [S] SUPPLY");
		}
	}

    public void GenerateCurrentMonster()
	{
		if(currentPlace.monsterNames.Count>0)
		{
			string newMonsterName = currentPlace.monsterNames[Random.Range(0,currentPlace.monsterNames.Count)];
			bool monsterFound = false;
			foreach (Monster monsterToPotentiallyFight in monsterList)
			{
				if (monsterToPotentiallyFight.name == newMonsterName)
				{
					currentMonster = monsterToPotentiallyFight;
					monsterFound = true;
					fighting=true;
				}
			}
			if (!monsterFound)
			{
				Debug.Log("ERROR: Monster not found.");
			}
		}
    }


    void RandomSponsorshipMedPacks()
    {

        int sponsorMedPacks = Random.Range(5, 20);
        medPacks += sponsorMedPacks;
        Print("Homebase has informed you that the mission has picked up a sponsor! You have been sent " + sponsorMedPacks + " medpacks.");
        PlayerStatus();
    }


    void FightMonster()
    {
        int fireLaser = 0; // number of shots fired
        if (currentWeaponLoot.ammoValue <= 0)
        {
            FiringLaserEmpty();
            fireLaser = 0;
        }
        else
        {
            fireLaser = 1;
            currentWeaponLoot.ammoValue -= fireLaser;
            Print("You used an Energy Cell! You now have " + currentWeaponLoot.ammoValue + " left!");
        }

        int damageTaken = Random.Range(1, currentMonster.damage);
        hitPoints -= damageTaken;
        Print("The " + currentMonster.name + " attacks. You have lost " + damageTaken + " HP");
        if (hitPoints <= 0)
        {
            Die();
        }
        else
        {
            if (fireLaser > 0)
            {
                int xpGained = currentMonster.xpGained;
                Print("You killed an alien. You gained: " + xpGained + " XP");
	            experiencePoints += xpGained;
	            
	            //Get currency
	            int currencyGained = (int)Random.Range(currentMonster.CurrencyMin,currentMonster.CurrencyMax);
	            Print("It had  " + currencyGained + " " + currencyName + " in its wallet");
	            currency += currencyGained;

                if (Random.Range(0, 100) <= currentMonster.lootDropChance)
                {
                    string nameOfLootDropped = currentMonster.lootDropNames[Random.Range(0, currentMonster.lootDropNames.Count)];
                    Loot newLoot = new Loot(nameOfLootDropped);
                    HandleNewLoot(newLoot);
                }
                if (Random.Range(0, 100) <= currentMonster.lootDropChance)
                {
                    medPacks++;
                    Print("You have found a Med Pack!");
                }
                if (Random.Range(2, 100) <= currentMonster.lootDropChance)
                {
                    currentWeaponLoot.ammoValue++;
                    Print("You have found an Energy Cell!");
                }

				

                if (experiencePoints >= xpNeededToLevel)
                {
                    LevelUp();
                }
	            
	            fighting = false;
	            PlaceMenuPrint();
            } // end if fired laser
            else
            {
                Print("You ran out of energy cells for your weapon and had to flee.");
            }
        } // end if not dead

        PlayerStatus();
    }

    public void HandleNewLoot(Loot newLoot)
    {
        if (newLoot.damageValue > 0)
        {
            if (newLoot.damageValue > currentWeaponLoot.damageValue)
            {
                // it is a BETTER weapon!
                currentWeaponLoot = newLoot;
                currentWeaponLoot.ammoValue = newLoot.ammoValue;
                Print("NEW WEAPON: " + newLoot.name + " AMMO: " + newLoot.ammoValue + " DAMAGE: " + newLoot.damageValue);
            }
            else if (newLoot.name == currentWeaponLoot.name)
            {
                Print("You found another " + newLoot.name + " and took " + newLoot.ammoValue + " ammo from it.");
                currentWeaponLoot.ammoValue += newLoot.ammoValue;
            }
        }
        if ( newLoot.name == currentWeaponLoot.ammoLootRequired)
        {
            currentWeaponLoot.ammoValue += newLoot.ammoValue;
            Print("AMMO +" + newLoot.ammoValue);
        }
        if ( newLoot.healValue>0)
        {
            medPacks += newLoot.healValue;
        }
        if (newLoot.pointsValue>0)
        { 
            Print("YOU FOUND " + newLoot.pointsValue + " YULETRONIUM");
            points += newLoot.pointsValue;
        }
    }

    void FiringLaserEmpty()
    {
        if (currentWeaponLoot.ammoValue != 0)
        {
            currentWeaponLoot.ammoValue = 0;
        }
        Print("You have no energy cells left!");

    }

    void Die()
    {
        if (hitPoints != 0)
        {
            hitPoints = 0;
        }
        Print("You have been eaten by an alien.");
        dead = true;
    }

    void Heal()
    {
        if (medPacks >= 1)
        {
            hitPoints = maxHitPoints;
            Print("You have been healed!");
            medPacks--;
        }
        else
        {
            Print("No Med Packs. Maybe I should check with Homebase and see if we have any new sponsors...");
        }
        PlayerStatus();
    }


    void LevelUp()
    {
        level++;
        xpNeededToLevel = 100 * level;
        Print("You have reached level " + level + "!");
        maxHitPoints += hpGainedPerLevel;
    }

    public void InitPlayer()
    {
        currentWeaponLoot = new Loot("Laser Pistol");
        
    }
	
	// Accepts input when you are fighting
	public void FightMenuInput()
	{
		
		if (!dead && printDelayedBufferString.Length<=5)
		{
			if (fighting)
			{
				if (Input.GetKeyDown(KeyCode.F))
				{
					FightMonster();
				}
				if (Input.GetKeyDown(KeyCode.H))
				{
					Heal();
				}
				if (Input.GetKeyDown(KeyCode.S))
				{
					RandomSponsorshipMedPacks();
				}
			}
			else
			{
				if (Event.current != null && Event.current.isKey)
				{
					PlaceMenuInput();
				}
			}
			
		}
	}
    #endregion methods



    #region Unity methods
    // Use this for initialization
    void Start()
    {
        consoleText = GameObject.Find("ConsoleText").GetComponent<Text>();
	    InitPlayer();
	    currentMonster=null;
	    GoToPlace("Start");
	    //PlayerStatus();
    }

    void Awake()
    {
        _Instance = this;
    }
	
	public bool fighting = false;
	
    // Update is called once per frame
    void Update()
    {
	    PrintBufferText();
	    PlaceMenuInput();
	    FightMenuInput();
    }
    #endregion Unity methods
}

