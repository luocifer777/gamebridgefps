﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
/// An exit defines a "direction" word and a destination's unique place name.
public class Exit
{
	public ExitDirection direction = ExitDirection.North;
	public string destination = "Test Planet";
}

public enum ExitDirection
{
	North,
	South,
	East,
	West,
	Up,
	Down,
	Hatch,
	Gate,
	Vent
}